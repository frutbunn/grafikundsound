'use strict'

/*
	Atari ST Grafik und Sound Demo

	A javascript remake of the famous Atari ST 'Grafik und Sound Demo' by Eckhard Kruse in 1986!
	http://www.eckhardkruse.net/atari_st/index.html
	http://www.eckhardkruse.net/atari_st/grafik.html

	All graphics and sound ripped with an Atari ST emulator, and are the copyright of Eckhard Kruse.


	Javascript remake by David Eggleston (aka frutbunn).
	http://frutbunn.tk/

	Using the CODEF framework by Antoine Santo (aka NoNameNo).
	http://codef.santo.fr/
	http://wab.com/

	With some borrowed CODEF stuff from Karl Cullen (aka Mellow Man).
	http://codef.namwollem.co.uk/index_non_ios.html

	This work is licensed under the Creative Commons Attribution 4.0 International License. 
	To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
*/

var animation = function() {

	// move2 is optional. Mix is optional and defaults to [1, 1] and will add move and move2 together.
	// Change numbers to only mix specific axis, percentages etc
	function blit(time, canvas, image, move, anim, tile, move2, mix) {
		var p = move.current(time);
		var a = tile.position(anim.current(time) );

		if (move2) 
			var p2 = move2.current(time);
		else
			var p2 = new fruCinema.Coordinate(0,0);

		if (!mix) mix=[1,1];

		p.x-= Math.floor(a.w*.5);
		p.y-= Math.floor(a.h*.5);
	
		image.drawPart(canvas, p.x+p2.x*mix[0], p.y+p2.y*mix[1], a.x, a.y, a.w, a.h);
	}

	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-

	var STAGE_OFFSET_X = 16;

	var scenery = {
		background1: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.background1.draw(canvas, 0, 0);
		},

		background2: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.background2.draw(canvas, 0, 0);
		},

		stage: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.stage.draw(canvas, STAGE_OFFSET_X+154, 146);
		},

		guitar: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.guitar.draw(canvas, STAGE_OFFSET_X+172, 216);
		},

		blueDudeSeated: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+444, 210);
			obj.image.blueDudeSeated.drawPart(canvas, p.x, p.y, 0, 0, 74, 138);
		},

		hairyDudeSeated: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+252, 138);
			obj.image.hairyDudeSeated.drawPart(canvas, p.x, p.y, 0, 0, 62, 86);
		},

		greenDudeSeated: function(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+16, 188);
			obj.image.greenDudeSeated.drawPart(canvas, p.x, p.y, 0, 0, 154, 148);
		}

	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene1(data) {
		var DURATION = 18.5;

		var scene = new fruCinema.Scene(DURATION, "Introduction");

		var BIRDY1 = 7;
		var BIRDY2 = 9.5;
		var BIRDY3 = 2;
		var SPEECH = 6;
		var FADE = 1.;
		var FLY_Y = 220;
		var SPEECH_P = new fruCinema.Coordinate(76, 8);

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			birdy1: new fruCinema.Tile(80, 80, 32),
			birdy2: new fruCinema.Tile(64, 64, 64),
			speech1: new fruCinema.Tile(388, 388, 222),
			fade: new fruCinema.Tile(388, 388, 222)
		};

		var anim = { // count, duration, loop, [indices]
			birdy1: new fruCinema.Animation(4, 1, true, [0, 1, 0, 2]),
			birdy2: new fruCinema.Animation(4, .7, true, [0, 1, 0, 2]),
			speech1: new fruCinema.Animation(11, SPEECH, false, [0, 0, 0, 1, 1, 2, 2, 2, 3, 4, 5]),
			fade: new fruCinema.Animation(6, FADE, false, [6, 7, 8, 9, 10, 11])
		};

		var move = { // [coordinates], duration, loop
			birdy1: new fruCinema.Move([-80, FLY_Y,  500, FLY_Y], BIRDY1, false),
			birdy1b: new fruCinema.Move([0, -4, 0, 4], 0.65, true),
			birdy2: new fruCinema.Move([500, FLY_Y-6,  500, FLY_Y], .7, true),
			birdy3: new fruCinema.Move([500, FLY_Y,  640, FLY_Y], 2, false)
		};


		scene.add(scenery.background1, "background", data, 0, DURATION);


		function birdy1(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.birdy1, move.birdy1, anim.birdy1, tile.birdy1, move.birdy1b, [0, 1]);
		} 
		scene.add(birdy1, "Birdy fly right", data, 0, BIRDY1);


		function birdy2(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.birdy2, move.birdy2, anim.birdy2, tile.birdy2);
		}
		scene.add(birdy2, "Birdy hover", data, BIRDY1, BIRDY2);


		function speech1(id, obj, time, duration) {
			var rb = obj.renderBuffer;

			var a = tile.speech1.position(anim.speech1.current(time) );
			obj.image.speech1.drawPart(rb, SPEECH_P.x, SPEECH_P.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(speech1, "Speech", data, BIRDY1+1, SPEECH);


		function fade(id, obj, time, duration) {
			var rb = obj.renderBuffer;

			var a = tile.speech1.position(anim.fade.current(time) );
			obj.image.speech1.drawPart(rb, SPEECH_P.x, SPEECH_P.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(fade, "Speech", data, BIRDY1+1+SPEECH, FADE);


		function birdy3(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.birdy1, move.birdy3, anim.birdy1, tile.birdy1, move.birdy1b, [0,1]);
		} 
		scene.add(birdy3, "Birdy fly right 2", data, BIRDY1+BIRDY2, BIRDY3);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene2(data) {
		var DURATION = 4;

		var scene = new fruCinema.Scene(DURATION, "Blue dude bouncing");

		var LEFT = -36;
		var RIGHT = 648;
		var WIDTH = RIGHT-LEFT;
		var JUMP_WIDTH = WIDTH/4;

		var GROUND = 264;
		var AIR = 200;

		var LATENCY = 0.;

		var LAND_DURATION = .5;
		var JUMP_DURATION = (DURATION/4-LAND_DURATION);
		var FULL_DURATION = (JUMP_DURATION+LAND_DURATION);

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			blueDude: new fruCinema.Tile(288, 72, 116)
		};

		var anim = { // count, duration, loop, [indices]
			blueDude1: new fruCinema.Animation(2, JUMP_DURATION, false, [2, 3]),
			blueDude2: new fruCinema.Animation(2, LAND_DURATION, false, [1, 0])
		};

		var move = { // [coordinates], duration, loop
			blueDudeJump1: new fruCinema.Move([(JUMP_WIDTH*0)+LEFT, GROUND,   (JUMP_WIDTH*0)+LEFT+(JUMP_WIDTH/2), AIR,   (JUMP_WIDTH*0)+LEFT+(JUMP_WIDTH), GROUND], JUMP_DURATION, false),
			blueDudeJump2: new fruCinema.Move([(JUMP_WIDTH*1)+LEFT, GROUND,   (JUMP_WIDTH*1)+LEFT+(JUMP_WIDTH/2), AIR,   (JUMP_WIDTH*1)+LEFT+(JUMP_WIDTH), GROUND], JUMP_DURATION, false),
			blueDudeJump3: new fruCinema.Move([(JUMP_WIDTH*2)+LEFT, GROUND,   (JUMP_WIDTH*2)+LEFT+(JUMP_WIDTH/2), AIR,   (JUMP_WIDTH*2)+LEFT+(JUMP_WIDTH), GROUND], JUMP_DURATION, false),
			blueDudeJump4: new fruCinema.Move([(JUMP_WIDTH*3)+LEFT, GROUND,   (JUMP_WIDTH*3)+LEFT+(JUMP_WIDTH/2), AIR,   (JUMP_WIDTH*3)+LEFT+(JUMP_WIDTH), GROUND], JUMP_DURATION, false)
		};


		scene.add(scenery.background1, "background", data, 0, DURATION);


		function blueDude1Jump(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			blit(time, canvas, obj.image.blueDude, move.blueDudeJump1, anim.blueDude1, tile.blueDude);
		} 
		scene.add(blueDude1Jump, "blueDude1Jump", data, LATENCY+FULL_DURATION*0, JUMP_DURATION );


		function blueDude1Land(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate((JUMP_WIDTH*1)+LEFT, GROUND, GROUND);
			var a = tile.blueDude.position(anim.blueDude2.current(time) );
			p.x-= Math.floor(a.w*.5);
			p.y-= Math.floor(a.h*.5);
			obj.image.blueDude.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(blueDude1Land, "blueDude1Land", data, LATENCY+FULL_DURATION*0+JUMP_DURATION, LAND_DURATION );


		function blueDude2Jump(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			blit(time, canvas, obj.image.blueDude, move.blueDudeJump2, anim.blueDude1, tile.blueDude);
		} 
		scene.add(blueDude2Jump, "blue dude 2Jump", data, LATENCY+FULL_DURATION*1, JUMP_DURATION );


		function blueDude2Land(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate((JUMP_WIDTH*2)+LEFT, GROUND, GROUND);
			var a = tile.blueDude.position(anim.blueDude2.current(time) );
			p.x-= Math.floor(a.w*.5);
			p.y-= Math.floor(a.h*.5);
			obj.image.blueDude.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(blueDude2Land, "blue dude 2Land", data, LATENCY+FULL_DURATION*1+JUMP_DURATION, LAND_DURATION );


		function blueDude3Jump(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			blit(time, canvas, obj.image.blueDude, move.blueDudeJump3, anim.blueDude1, tile.blueDude);
		} 
		scene.add(blueDude3Jump, "blue dude 3Jump", data, LATENCY+FULL_DURATION*2, JUMP_DURATION );


		function blueDude3Land(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate((JUMP_WIDTH*3)+LEFT, GROUND, GROUND);
			var a = tile.blueDude.position(anim.blueDude2.current(time) );
			p.x-= Math.floor(a.w*.5);
			p.y-= Math.floor(a.h*.5);
			obj.image.blueDude.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(blueDude3Land, "blue dude 3Land", data, LATENCY+FULL_DURATION*2+JUMP_DURATION, LAND_DURATION );


		function blueDude4Jump(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			blit(time, canvas, obj.image.blueDude, move.blueDudeJump4, anim.blueDude1, tile.blueDude);
		} 
		scene.add(blueDude4Jump, "blue dude 4Jump", data, LATENCY+FULL_DURATION*3, JUMP_DURATION );


		function blueDude4Land(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate((JUMP_WIDTH*4)+LEFT, GROUND, GROUND);
			var a = tile.blueDude.position(anim.blueDude2.current(time) );
			p.x-= Math.floor(a.w*.5);
			p.y-= Math.floor(a.h*.5);
			obj.image.blueDude.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(blueDude4Land, "blue dude 4Land", data, LATENCY+FULL_DURATION*3+JUMP_DURATION, LAND_DURATION );


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene3(data) {
		var DURATION = 6;

		var scene = new fruCinema.Scene(DURATION, "Hairy dude walking");

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			hairyDude: new fruCinema.Tile(252, 84, 128)
		};

		var anim = { // count, duration, loop, [indices]
			hairyDude: new fruCinema.Animation(4, .75, true, [0, 1, 0, 2])
		};

		var move = { // [coordinates], duration, loop
			hairyDude: new fruCinema.Move([-42, 258, 640, 258], 6, false)
		};


		scene.add(scenery.background1, "background", data, 0, DURATION);


		function hairyDude(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.hairyDude, move.hairyDude, anim.hairyDude, tile.hairyDude);
		} 
		scene.add(hairyDude, "hairy dude walk right", data, 0, DURATION);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene4(data) {
		var DURATION = 3.5;

		var scene = new fruCinema.Scene(DURATION, "Green dude skating");

		var START = -20;
		var END = 650; //628;
		var WIDTH = END-START;
		var PUSHES = 20;

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			greenDude: new fruCinema.Tile(324, 108, 150)
		};

		var anim = { // count, duration, loop, [indices]
			greenDude: new fruCinema.Animation(5, DURATION/5, true, [0, 1, 2, 2, 1])
		};

		var move = { // [coordinates], duration, loop
			greenDude: new fruCinema.Move([
				START+WIDTH/PUSHES*0, 0,         START+WIDTH/PUSHES*1,0,
				START+WIDTH/PUSHES*2, 75,         START+WIDTH/PUSHES*3,0,
				START+WIDTH/PUSHES*4, 0,         START+WIDTH/PUSHES*5,0,
				START+WIDTH/PUSHES*6, 75,         START+WIDTH/PUSHES*7,0,

				START+WIDTH/PUSHES*8, 0,         START+WIDTH/PUSHES*9,0,
				START+WIDTH/PUSHES*10, 75,         START+WIDTH/PUSHES*11,0,
				START+WIDTH/PUSHES*12, 0,         START+WIDTH/PUSHES*13,0,
				START+WIDTH/PUSHES*14, 75,         START+WIDTH/PUSHES*15,0,

				START+WIDTH/PUSHES*16, 0,         START+WIDTH/PUSHES*17,0,
				START+WIDTH/PUSHES*18, 75,         START+WIDTH/PUSHES*19,0,

				], DURATION, false)
		};


		scene.add(scenery.background1, "background", data, 0, DURATION);


		function greenDude(id, obj, time, duration) {
			var canvas = obj.renderBuffer;

			var p = move.greenDude.current(time);
			var a = tile.greenDude.position(anim.greenDude.current(time) );

			p.x-= Math.floor(a.w*.5);
			p.y = 172;

			obj.image.greenDude.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);

		} 
		scene.add(greenDude, "green dude skate right", data, 0, DURATION);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene5(data) {
		var DURATION = 5+1;

		var scene = new fruCinema.Scene(DURATION, "Blue dude to stage");

		var LEFT = -36*2;
		var RIGHT = STAGE_OFFSET_X+648;
		var WIDTH = RIGHT-LEFT;
		var JUMP_WIDTH = WIDTH/4;

		var GROUND = 264;
		var AIR = 190;

		var LATENCY = 0.;

		var LAND_DURATION1 = .5;
		var LAND_DURATION2 = .5;

		var JUMP_DURATION1 = (1.5-LAND_DURATION1);
		var JUMP_DURATION2 = (4.-LAND_DURATION2);

		var FULL_DURATION1 = (JUMP_DURATION1+LAND_DURATION1);
		var FULL_DURATION2 = (JUMP_DURATION2+LAND_DURATION2);

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			blueDude: new fruCinema.Tile(288, 72, 116)
		};

		var anim = { // count, duration, loop, [indices]
			blueDude1: new fruCinema.Animation(2, JUMP_DURATION1, false, [2, 3]),
			blueDude1b: new fruCinema.Animation(2, JUMP_DURATION2, false, [2, 3]),
			blueDude2: new fruCinema.Animation(2, LAND_DURATION2, false, [1, 0]),
			blueDudeSeated: new fruCinema.Animation(2, 10, true, [0, 0])
		};

		var move = { // [coordinates], duration, loop
			blueDudeJump1: new fruCinema.Move([
				 (JUMP_WIDTH*0)+LEFT, GROUND
				 , ((JUMP_WIDTH*0)+LEFT+(JUMP_WIDTH/2)), AIR
				 , ((JUMP_WIDTH*0)+LEFT+(JUMP_WIDTH)), GROUND
				 ], JUMP_DURATION1, false),

			blueDudeJump2: new fruCinema.Move([
				 ((JUMP_WIDTH*0)+LEFT+(JUMP_WIDTH)), GROUND,
				 (JUMP_WIDTH*1)+LEFT+(JUMP_WIDTH/2)*(2.1-1), AIR-(125-20),

				 (JUMP_WIDTH*1)+LEFT+(JUMP_WIDTH/2)*(1.5), AIR-(140-20),
				 (JUMP_WIDTH*1)+LEFT+(JUMP_WIDTH/2)*(1.9), AIR-(150-20),
				 (JUMP_WIDTH*1)+LEFT+(JUMP_WIDTH/2)*(2.1), AIR-(140-20),

				 (JUMP_WIDTH*1)+LEFT+(JUMP_WIDTH)*2.1, GROUND
				 ], JUMP_DURATION2, false)
		};


		scene.add(scenery.background2, "background", data, 0, DURATION);
		scene.add(scenery.stage, "stage", data, 0, DURATION);
		scene.add(scenery.guitar, "guitar", data, 0, DURATION);


		function blueDude1Jump(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			blit(time, canvas, obj.image.blueDude, move.blueDudeJump1, anim.blueDude1, tile.blueDude);
		} 
		scene.add(blueDude1Jump, "blue dude 1Jump", data, LATENCY+FULL_DURATION1*0, JUMP_DURATION1 );


		function blueDude1Land(id, obj, time, duration) {
			var canvas = obj.renderBuffer;

			var p = new fruCinema.Coordinate(((JUMP_WIDTH*0)+LEFT+(JUMP_WIDTH)), GROUND, GROUND);
			var a = tile.blueDude.position(anim.blueDude2.current(time) );
			p.x-= Math.floor(a.w*.5);
			p.y-= Math.floor(a.h*.5);

			obj.image.blueDude.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);
		} 
		scene.add(blueDude1Land, "blue dude 1Land", data, LATENCY+FULL_DURATION1*0+JUMP_DURATION1, LAND_DURATION1 );


		function blueDude2Jump(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			blit(time, canvas, obj.image.blueDude, move.blueDudeJump2, anim.blueDude1b, tile.blueDude);
		} 
		scene.add(blueDude2Jump, "blue dude 2Jump", data, LATENCY+FULL_DURATION1*1, JUMP_DURATION2 );


		function blueDude2Land(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+444, 210);
			obj.image.blueDudeSeated.drawPart(canvas, p.x, p.y, 0, 0, 74, 138);
		} 
		scene.add(blueDude2Land, "blue dude 2Land", data, LATENCY+FULL_DURATION1*1+JUMP_DURATION2, LAND_DURATION2+10 );


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene6(data) {
		var DURATION = 4;

		var scene = new fruCinema.Scene(DURATION, "Hairy dude to stage");

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			hairyDude: new fruCinema.Tile(252, 84, 128)
		};

		var anim = { // count, duration, loop, [indices]
			hairyDude: new fruCinema.Animation(4, .75, true, [0, 1, 0, 2])
		};

		var move = { // [coordinates], duration, loop
			hairyDude: new fruCinema.Move([-42, 218, STAGE_OFFSET_X+280, 218], 3, false)
		};


		scene.add(scenery.background2, "background", data, 0, DURATION);


		function hairyDude(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.hairyDude, move.hairyDude, anim.hairyDude, tile.hairyDude);
		} 
		scene.add(hairyDude, "hairy dude walk right", data, 0, 3);


		scene.add(scenery.stage, "stage", data, 0, DURATION);
		scene.add(scenery.guitar, "guitar", data, 0, DURATION);
		scene.add(scenery.blueDudeSeated, "blue dude seated", data, 0, DURATION);
		scene.add(scenery.hairyDudeSeated, "hairy dude seated", data, 3, DURATION-3);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene7(data) {
		var DURATION = 4;

		var scene = new fruCinema.Scene(DURATION, "Green dude to stage");

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			greenDude: new fruCinema.Tile(324, 108, 150),
			skateboard: new fruCinema.Tile(72, 72, 20)
		};

		var anim = { // count, duration, loop, [indices]
			greenDude: new fruCinema.Animation(8, 2, false, [0, 0, 0, 0, 0, 0, 0, 2]),
			skateboard: new fruCinema.Animation(2, 1, true, [0, 0])
		};

		var move = { // [coordinates], duration, loop
			greenDude: new fruCinema.Move([-42, 258, STAGE_OFFSET_X+100, 258], 2, false),
			skateboard: new fruCinema.Move([76, 324, STAGE_OFFSET_X+500, 390, STAGE_OFFSET_X+700, 390], 2, false)
		};


		scene.add(scenery.background2, "background", data, 0, DURATION);
		scene.add(scenery.stage, "stage", data, 0, DURATION);
		scene.add(scenery.guitar, "guitar", data, 0, 2);
		scene.add(scenery.blueDudeSeated, "blue dude seated", data, 0, DURATION);
		scene.add(scenery.hairyDudeSeated, "hairy dude seated", data, 0, DURATION);


		function greenDude(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.greenDude, move.greenDude, anim.greenDude, tile.greenDude);
		} 
		scene.add(greenDude, "green dude to stage", data, 0, 2);


		function skateboard(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.skateboard, move.skateboard, anim.skateboard, tile.skateboard);
		} 
		scene.add(skateboard, "skateboard", data, 2, 4);


		scene.add(scenery.greenDudeSeated, "green dude seated", data, 2, DURATION-2);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene8(data) {
		var DURATION = 8-3;

		var scene = new fruCinema.Scene(DURATION, "Birdy to stage");

		var FLY_Y = 120;

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			birdy1: new fruCinema.Tile(80, 80, 32)
		};

		var anim = { // count, duration, loop, [indices]
			birdy1: new fruCinema.Animation(4, 1, true, [0, 1, 0, 2])
		};

		var move = { // [coordinates], duration, loop
			birdy1: new fruCinema.Move([-80, FLY_Y,  STAGE_OFFSET_X+380, FLY_Y], 6-1, false),
			birdy1b: new fruCinema.Move([0, -4, 0, 4], 0.65, true)
		};


		scene.add(scenery.background2, "background", data, 0, DURATION);
		scene.add(scenery.stage, "stage", data, 0, DURATION);
		scene.add(scenery.blueDudeSeated, "blue dude seated", data, 0, DURATION);
		scene.add(scenery.hairyDudeSeated, "hairy dude seated", data, 0, DURATION);
		scene.add(scenery.greenDudeSeated, "green dude seated", data, 0, DURATION);


		function birdy1(id, obj, time, duration) {
			var rb = obj.renderBuffer;
			blit(time, rb, obj.image.birdy1, move.birdy1, anim.birdy1, tile.birdy1, move.birdy1b, [0, 1]);
		} 
		scene.add(birdy1, "birdy fly right", data, 0, 6-1);


		return scene;
	}


	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-


	function scene9(data) {	
		var DURATION = 144;

		var ANIM_DURATION = 143;
		var ANIM_COUNT = 7157;

		var scene = new fruCinema.Scene(DURATION, "Animation");

		var tile = { // canvasWidth, tileWidth, tileHeight, horizontalTileCount
			allesklar: new fruCinema.Tile(322, 322, 84),

			blueDudeHead: new fruCinema.Tile(1840, 92, 42),
			blueDudeArms: new fruCinema.Tile(1840, 92, 48),
			blueDudeLegs: new fruCinema.Tile(1840, 92, 60),

			greenDudeHead: new fruCinema.Tile(3080, 154, 56),
			greenDudeArms: new fruCinema.Tile(3080, 154, 48),
			greenDudeLegs: new fruCinema.Tile(3080, 154, 50),

			hairyDudeA: new fruCinema.Tile(1400, 70, 136),
			hairyDudeB: new fruCinema.Tile(1880, 94, 136),
			hairyDudeC: new fruCinema.Tile(1400, 70, 136)
		};

		var anim = { // count, duration, loop, [indices]
			allesklar: new fruCinema.Animation(8, 3.3, false, [0,0,0,1,2,3,4,4]),

			blueDudeHead: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.blueDudeHead() ),
			blueDudeArms: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.blueDudeArms() ),
			blueDudeLegs: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.blueDudeLegs() ),

			greenDudeHead: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.greenDudeHead() ),
			greenDudeArms: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.greenDudeArms() ),
			greenDudeLegs: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.greenDudeLegs() ),

			hairyDudeA: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.hairyDudeA() ),
			hairyDudeB: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.hairyDudeB() ),
			hairyDudeC: new fruCinema.Animation(ANIM_COUNT, ANIM_DURATION, false, animationData.hairyDudeC() )
		};

		var move = { // [coordinates], duration, loop
		};


		scene.add(scenery.background2, "background", data, 0, DURATION);
		scene.add(scenery.stage, "stage", data, 0, DURATION);


		function blueDudePlaying(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+426, 198);

			var a = tile.blueDudeHead.position(anim.blueDudeHead.current(time) );
			obj.image.blueDudeHead.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);

			a = tile.blueDudeArms.position(anim.blueDudeArms.current(time) );
			obj.image.blueDudeArms.drawPart(canvas, p.x, p.y+42, a.x, a.y, a.w, a.h);

			a = tile.blueDudeLegs.position(anim.blueDudeLegs.current(time) );
			obj.image.blueDudeLegs.drawPart(canvas, p.x, p.y+90, a.x, a.y, a.w, a.h);

		} 
		scene.add(blueDudePlaying, "blue dude playing", data, 0, DURATION);


		function greenDudePlaying(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+16, 188);

			var a = tile.greenDudeHead.position(anim.greenDudeHead.current(time) );
			obj.image.greenDudeHead.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);

			a = tile.greenDudeArms.position(anim.greenDudeArms.current(time) );
			obj.image.greenDudeArms.drawPart(canvas, p.x, p.y+56, a.x, a.y, a.w, a.h);

			a = tile.greenDudeLegs.position(anim.greenDudeLegs.current(time) );
			obj.image.greenDudeLegs.drawPart(canvas, p.x, p.y+104, a.x, a.y, a.w, a.h);

		} 
		scene.add(greenDudePlaying, "green dude playing", data, 0, DURATION);


		function hairyDudePlaying(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+172, 102);

			var a = tile.hairyDudeA.position(anim.hairyDudeA.current(time) );
			obj.image.hairyDudeA.drawPart(canvas, p.x, p.y, a.x, a.y, a.w, a.h);

			a = tile.hairyDudeB.position(anim.hairyDudeB.current(time) );
			obj.image.hairyDudeB.drawPart(canvas, p.x+70, p.y, a.x, a.y, a.w, a.h);

			a = tile.hairyDudeC.position(anim.hairyDudeC.current(time) );
			obj.image.hairyDudeC.drawPart(canvas, p.x+164, p.y, a.x, a.y, a.w, a.h);

		} 
		scene.add(hairyDudePlaying, "hairy dude playing", data, 0, DURATION);


		function blueDudeSpeech(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.speech2.draw(canvas, STAGE_OFFSET_X+356, 12);
		} 
		scene.add(blueDudeSpeech, "blue dude speech", data, 3.1, 2.7);


		function hairyDudeSpeech(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.speech3.draw(canvas, STAGE_OFFSET_X+156, 22);
		}
		scene.add(hairyDudeSpeech, "hairy dude speech", data, 16, 3);


		function greenDudeSpeech(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			obj.image.speech4.draw(canvas, STAGE_OFFSET_X+184, 24);
		} 
		scene.add(greenDudeSpeech, "green dude speech", data, 36.3, 2.5);


		function allesklar(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var a = tile.allesklar.position(anim.allesklar.current(time) );
			obj.image.allesklar.drawPart(canvas, STAGE_OFFSET_X+76, 22, a.x, a.y, a.w, a.h);
		} 
		scene.add(allesklar, "allesklar", data, 57.5, 3.3);


		function yeah(id, obj, time, duration) {
			var canvas = obj.renderBuffer;
			var p = new fruCinema.Coordinate(STAGE_OFFSET_X+156, 82);
			obj.image.yeah.draw(canvas, p.x, p.y);
		}
		scene.add(yeah, "Yeah 1", data, 21.85, 1);
		scene.add(yeah, "Yeah 2", data, 80.5, 1);


		function fade(id, obj, time, duration) {
			var fade = Math.floor(time%2*10)/20;

			console.log(fade)

			obj.ctx.fillStyle = "rgba(0,0,0," + fade + ")";
			obj.ctx.fillRect(0, 0, obj.ctx.canvas.width, obj.ctx.canvas.height);
			obj.ctx.fillStyle = "rgba(1,1,1,1)";
		} 
		scene.add(fade, "fade", data, DURATION-2, 2);


		return scene;
	}

	//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-

	function make(data) {
		film.add(scene1(data) );
		film.add(scene2(data) );
		film.add(scene3(data) );
		film.add(scene4(data) );
		film.add(scene5(data) );
		film.add(scene6(data) );
		film.add(scene7(data) );
		film.add(scene8(data) );
		film.add(scene9(data) );
	}

	return {
		make: make
	}
}();
