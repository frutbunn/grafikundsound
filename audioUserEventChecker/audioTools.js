'use strict'

//	-	-	-
//	The test user gesture required for Audio code:
//	-	-	-

var AudioAPItools = (function() {
	var api = {}

	function createAudioContext() {
		var ctx = null;
		try {
			var _audioContext = window.AudioContext || window.webkitAudioContext;
			ctx = new _audioContext();
		} catch(err) {
			console.error(err);
		} 

		return ctx;
	}

	api.const = Object.freeze({
		suspendedTimeout: 1000
	});

	api.haveAudioAPI = function AudioAPItools_haveAudioAPI() {
		var ctx = createAudioContext();

		if (ctx) {
			ctx.close().then(function(){});
			return true;
		} else {
			return false;
		}
	}

	// Generating an Audio context is async on some browsers, so you must supply a callback
	api.needUserGesture =  function AudioAPItools_needUserGesture(callback) {
		if (! callback) {
			throw "User callback is required.";
		}

		var ctx = createAudioContext();
		if (! ctx) {
			throw "Audio API is not supported for this browser.";
		}

		//	-	-	-

		function _finish(needUserGesture) {
			// Remove onstatechange event, Chrome needs this otherwise the promise below fails.
			ctx.onstatechange = null;

			ctx.close().then(function() {
				setTimeout(function() {
					callback(needUserGesture);
				}, 0);
			});
		}

		//	-	-	-

		if (ctx.state==="running") {
			// If context created in 'running' state immediately (sound enabled for this site without user gesture), then
			// return the good news.
			_finish(false);
		} else {
			// Chrome creates an audio context in 'running' mode immediately (I think) if it can, and in 'suspended' mode
			// if a user event is required. BUT, it does NOT also generate an 'onstatechange' event / at least for me...
			//
			// So, test for Chrome user gesture changes using a timeout to give Firefox time to set the context status to 
			// running and call the onstatechange event handler above.
			//
			// With this approach even if Chrome or other browsers *sometimes* have a delay and call the the .onstatechange
			// event, everything should still work.
			var _timeout = setTimeout(function() {
				if (ctx.state==="running") {
					_finish(false);
				} else {
					// If we've got this far and the audio context is still suspended, assume we need a user gesture
					_finish(true);
				}
			}, api.const.suspendedTimeout);

			// Firefox (and maybe other browsers) creates an audio context in 'suspended' mode, then changes it to running.
			// So, test for that change in the 'onstatechange' event.
			ctx.onstatechange = function() {
				if (ctx.state==="running") {
					// Clear timeout above as onstatechange event has given us a positive answer to our question.
					if (_timeout) {
						try {
							clearTimeout(_timeout);
						} catch (err) {} // arg for edge
					}

					_finish(false);
				}
			}
			
		}

	}


	return api;
})();