'use strict'

/*
By Dave Eggleston (frutbunn) frutbunn@gmail.com

A 'film' animation library - Version: 0.2a

This work is licensed under the Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
*/

var fruCinema = function() { 

	var tools = function() {
		function tile(frame, horizontalTileCount) {
			var r = new Coordinate(Math.floor(frame%horizontalTileCount), Math.floor(frame/horizontalTileCount) );
			
			return r;
		}

		function length(p1, p2) {
			var n = new Coordinate(p2.x - p1.x, p2.y - p1.y);

			return Math.abs( Math.sqrt(n.x*n.x + n.y*n.y) );
		}

		function interpolate(p1, p2, percent) {
			var n = new Coordinate(p1.x + (p2.x - p1.x) * percent, p1.y + (p2.y - p1.y) * percent);

			return n;
		}

		return {
			length: length,
			interpolate: interpolate,
			tile: tile
		};
	}();

	function Bob(x, y, w, h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	function Tile(canvasWidth, tileWidth, tileHeight, horizontalTileCount) {
		var state = {
			horizontalTileCount: null,

			tileSize: {
				width: null,
				height: null
			}
		};

		function constructor() {
			if (horizontalTileCount)
				state.horizontalTileCount = horizontalTileCount;
			else
				state.horizontalTileCount = Math.floor(canvasWidth/tileWidth);

			state.tileSize.height = Number(tileHeight);
			state.tileSize.width = Number(tileWidth);
		}

		function position(frame) {
			var tile = tools.tile(frame, state.horizontalTileCount);

			tile.x*= state.tileSize.width;
			tile.y*= state.tileSize.height;

			return new Bob(tile.x, tile.y, state.tileSize.width, state.tileSize.height);
		}

		constructor();

		return {
			get width() { return state.tileSize.width; },
			get height() { return state.tileSize.height; },

			position: position
		}
	}


	function Coordinate(x, y) {
		this.x = x;
		this.y = y;
	}


	function Move(coordinates, duration, loop) {
		var that = this;

		var state = {
			len: null,
			percent: null,
			list: null,

			step: null,
			duration: null,
			loop: null,

			previous: {
				i: null,
				k: null,
				j: null,

				t: null
			}

		};

		function constructor() {
			// Calc list of Coordinate objects from coordinates
			if (!coordinates || coordinates.constructor !== Array || !coordinates.length%2)
				throw new Error("'coordinates' argument is incorrect. Must be an array of even length.");

			state.list = [];
			for(var i=0; i<coordinates.length; i+=2)
				state.list.push(new Coordinate(coordinates[i], coordinates[i+1]) );

			// If looping or only one coordinate supplied, attach first coordinate to end
			if (loop || state.list.length==1) state.list.push(state.list[0]);

			// Calc length array
			state.len = [];
			for(var i=0+1; i<state.list.length; i++)
				state.len.push(tools.length(state.list[i-1], state.list[i]) );

			// Calc step
			for(var i=0, k=0; i<state.len.length; i++)
				k+=state.len[i];
			state.step = (k == 0 ? 0 : 1/k);

			// Calc percentage array
			state.percent = [];
			for(var i in state.len)
				state.percent.push( state.len[i]*state.step );

			// Store duration and loop flag
			state.duration = duration;
			state.loop = loop ? true : false;

			// Default previous values to start values
			state.previous.t = 0;

			state.previous.i = 1;
			state.previous.k = state.percent[0];
			state.previous.j = 0;
		}

		function getCurrentCoordinate(currentTimeInSeconds) {
			// current time - Fixed to duration boundry, and always positive number
			var ct = Math.max(0., currentTimeInSeconds);
			if (state.loop)
				ct %= state.duration;

			// current percentage
			var cp = state.duration == 0 ? 0 : Math.min(ct/state.duration, 1);

			// Handle reset
			if (ct<=state.previous.t) {
				state.previous.i = 1;
				state.previous.k = state.percent[0];
				state.previous.j = 0;
			}

			// i Index within percent array
			// k Current percentage at end of line
			// j Current percentage at start of line
			var i=state.previous.i;
			var k=state.previous.k;
			var j=state.previous.j;
			for(;i<=state.percent.length-1; i++) {
				if (cp<=k) break;

				k+= state.percent[i];
				j+= state.percent[i-1];
			}

			// Store i, k, j, ct for next call
			state.previous.i = i;
			state.previous.k = k;
			state.previous.j = j;

			state.previous.t = ct;

			// new current time - within current line
			var nct = ct - state.duration*j;

			// new duration - within current line
			var nd = state.duration*state.percent[i-1];

			// relative current percentage - For interpolation of current point on current line
			// if new duration is zero, relative current percentage is zero
			var rcp = Math.min(1, nd == 0 ? 0 : nct/nd);

			// new point - the current position based on currentTimeInSeconds
			var np = tools.interpolate(state.list[i-1], state.list[i], rcp);
			np = new Coordinate(Math.floor(np.x), Math.floor(np.y));

			return np;
		}

		constructor();

		return {
			current: getCurrentCoordinate
		};
	}


	function Animation(count, duration, loop, indices) {
		var state = {
			count: null,
			duration: null,
			loop: null,
			indices: null
		};

		function constructor() {
			if (indices instanceof Array)
				state.indices = [].concat(indices);
			else
				state.indices = null;

			state.count = count;
			state.duration = duration;
			state.loop = loop ? true : false;
		}

		function currentFrame(currentTimeInSeconds) {
			// current time - Fixed to duration boundry, and always positive number
			var ct = Math.max(0., currentTimeInSeconds);
			
			if (!state.loop && ct>=state.duration)
				if (state.indices) 
					return state.indices[state.count-1];
				else
					return state.count-1;

			else
				ct %= state.duration;

			// current percentage - if duration is zero, fix to zero
			var cp = state.duration == 0 ? 0 : Math.min(ct/state.duration, 1);

			// current frame
			var cf = Math.floor((state.count)*cp);

			// If indices supplied, return that, otherwise return frame number
			if (state.indices)
				return state.indices[cf];
			else
				return cf;
		}

		constructor();

		return {
			current: currentFrame,
			get count() {return state.count; },
			get duration() {return state.duration; }
		};
	}


	function Film(name, loop, ctx) {

		function Entry(scene,  start) {
			this.scene = scene;
			this.start = start;
		}


		var state = {
			name: null,
			loop: null,

			previous: {
				cft: null,
				i: null
			},

			filmLength: null,

			scenes: null // array of Entry objects (above)
		};

		function constructor() {
			state.name = name;
			state.loop = loop ? true : false;

			state.scenes = [];

			state.filmLength = 0;

			state.previous.cft = 0;
			state.previous.i = 0;
		}

		function addScene(scene) {
			var o = new Entry(scene, state.filmLength);
			state.scenes.push(o);

			state.filmLength+= scene.duration;

			return true;
		}

		// Remove this
		function showOutput(ctx, output) {

			ctx.fillStyle="#00f";
			ctx.fillRect(0,0,ctx.canvas.width, ctx.canvas.height);

			ctx.font = "12" + 'pt courier';
			ctx.lineWidth = 3;
			ctx.strokeStyle = 'black';
			ctx.fillStyle = "#FFF";

			for(var i=0, y=12; i<output.length; i++, y+=12) {
				ctx.strokeText(output[i], 2, y);
				ctx.fillText(output[i], 2, y);
			}

		}

		function nextFrame(currentTimeInSeconds, ctx) {
			var output = [];

			var cft = Math.max(0., currentTimeInSeconds);

			var cfl = state.filmLength;
			if (cfl>0 && state.loop)
				cft %= cfl;

			if (cft <= state.previous.cft)
				state.previous.i = 0;

			for(var i=state.previous.i; i<state.scenes.length; i++) {
				var slice = state.scenes[i];
				var scene = slice.scene;
				if (cft >=slice.start && cft<slice.start+scene.duration) {
					
					var cst = cft - slice.start;
					
					// BANNER TEXT ---

					var header = "";
					if (state.name) header = "Film: [" + state.name + "] - ";
					header += "[" + (scene.name ? "scene " + (Number(i)+1) + " - "
					 + scene.name : "scene #" + (Number(i)+1)) + "]"
					output.push(header);

					output.push("Timers - Scene: " + Number(Math.floor( (cst)*100)/100).toFixed(2)
					 + " Film: " + Number(Math.floor( (cft)*100)/100).toFixed(2)
					 + " Global: " + Number(Math.floor( (currentTimeInSeconds)*100)/100).toFixed(2)
					);

					// Call the scene object nextFrame method
					output = output.concat( scene.nextFrame(cst) );
					// ---

					break;
				}

			}
			state.previous.i = i;
			state.previous.cft = cft;

			if (ctx)
				showOutput(ctx, output);

			return output;
		}

		constructor();

		return {
			add: addScene,
			nextFrame: nextFrame
		}
	}

	function Scene(duration, name) {
		// Scene is an array of Event objects, when runEvents is called, it scans
		// through the Event objects and executes each event based on currentTimeInSeconds.
		function Event(callback, userID, userVariable, start, duration, zOrder, parentScene) {
			var state = {
				parentScene: null,

				start: null,
				duration: null,
			
				callback: null,

				userID: null,
				userVariable: null,

				zOrder: null
			};

			state.parentScene = parentScene;
			state.zOrder = zOrder;
			state.callback = callback;

			state.start = start>=0 ? start : 0;
			state.duration = duration ? duration : null;

			state.userID = userID ? userID : null;
			state.userVariable = userVariable ? userVariable : null;

			return {
				state: state,

				get start() {return state.start; },
				get duration() {return state.duration; },

				get callback() {return state.callback; },

				get id() {return state.userID; },
				get variable() {return state.userVariable; },

				get parent() {return state.parentScene; },

				get zOrder() {return state.zOrder; },
				set zOrder(x) { state.zOrder = x; state.parentScene.reorderEvents(); }
			};
		}


		var state = {
			zOrderCount: null,

			name: null,
			duration: null,

			events: null,
		}

		function constructor() {
			state.zOrderCount = 0; // Incremented every time an event is added.

			state.name = name ? name : "";
			state.duration = duration;
			state.events = [];
		}

		function addEvent(callback, userID, userVariable, start, duration) {
			if (! callback instanceof Function)
				return false;

			var o = new Event(callback, userID, userVariable, start, duration, state.zOrderCount, api);
			state.events.push(o);

			state.zOrderCount++;

			return o;
		}

		function runEvents(currentSceneTimeInSeconds) {
			var output = [];

			var cst = Math.max(0., currentSceneTimeInSeconds);

			for(var i in state.events) {

				var e = state.events[i];
				
				// e.start defaults to 0 if not supplied to addEvent() above
				if (cst>=e.start && (cst<e.start+e.duration || e.duration === null) ) {

					// BANNER TEXT ---

					output.push("Event: " + Number(Math.floor( (cst-e.start)*100)/100).toFixed(2) 
					 + " zOrder: " + e.zOrder + " [" + e.id + "]");

					// ---

					var callback = e.callback;
					callback(e.id, e.variable, cst-e.start, e.duration);
				}
			
			}

			return output;
		}

		function reorderEvents() {

			// Resort events
			state.events.sort(function (a, b) {
				if(a.zOrder > b.zOrder) return 1;
				if(a.zOrder < b.zOrder) return -1;

				return 0;
			});
		}

		constructor();

		var api = {
			get name() {return state.name; },
			get duration() {return state.duration; }, 
			add: addEvent,
			nextFrame: runEvents,

			reorderEvents: reorderEvents
		}

		return api;
	}


	return {
		tools: tools,

		Coordinate: Coordinate,
		Move: Move,
		Animation: Animation,
		Tile: Tile,

		Film: Film,	
		Scene: Scene
	}
}();
