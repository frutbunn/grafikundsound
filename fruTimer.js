'use strict'

/*
By Dave Eggleston (frutbunn) frutbunn@gmail.com

A simple timer class - Version: 0.7

This work is licensed under the Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
*/

function fruTimer() {
	var start = null;
	var current = null;

	function currentTimeInMilliseconds() {
		if (start === null) 
			start = +new Date();
       	
	    current = (+new Date() ) - start;

	    return current;
	}

	function currentTimeInSeconds() {
		return currentTimeInMilliseconds() / 1000;
	}

	function setTimer(newTime) {
		current = newTime;
		catchupTimer();
	}

	function resetTimer() {
	    start = current = null;
	}

	// Calling this is optional, timer will also start when currentTimeInMilliseconds/currentTimeInSeconds is called
	function startTimer() {
		var _tmp = currentTimeInMilliseconds();
	}

	function catchupTimer() {
    	start = (+new Date() ) - current;
	}

	function derivedTimer(duration, min, max, func) {
		return ( ( (func() % duration) * (max - min) ) / duration ) + min;
	}

	return {
		seconds: {
			get current() { return currentTimeInSeconds(); },
			set current(x) { setTimer(x*1000); },
			
			newTimer: function(seconds, min, max) {return derivedTimer(seconds, min, max, currentTimeInSeconds); }
		},

		milliseconds: {
			get current() { return currentTimeInMilliseconds(); },
			set current(x) { setTimer(x); },

			newTimer: function(milliseconds, min, max) {return derivedTimer(milliseconds, min, max, currentTimeInMilliseconds); }
		},
		
		start: startTimer,
		reset: resetTimer,
		catchup: catchupTimer
	}	

}